package itacademy.com.project006;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etFirstName, etLastName;
    private Button btnSave, btnGet, btnClear;
    private TextView tvResult;

    private SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sqLiteHelper = new SQLiteHelper(this);

        tvResult = findViewById(R.id.tvResult);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);

        btnSave = findViewById(R.id.btnSave);
        btnGet = findViewById(R.id.btnGet);
        btnClear = findViewById(R.id.btnClear);

        btnSave.setOnClickListener(this);
        btnGet.setOnClickListener(this);
        btnClear.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                String firstName = etFirstName.getText().toString();
                String lastName = etLastName.getText().toString();
                if (!firstName.isEmpty() && !lastName.isEmpty()) {
                    sqLiteHelper.saveData(firstName, lastName);
                } else {
                    Toast.makeText(this, "Data is empty", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnGet:
                String result = sqLiteHelper.getData();
                tvResult.setText(result);
                break;
            case R.id.btnClear:
                sqLiteHelper.clearData();
                break;
        }
    }
}
